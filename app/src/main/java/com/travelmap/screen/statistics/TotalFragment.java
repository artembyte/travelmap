package com.travelmap.screen.statistics;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.travelmap.R;
import com.travelmap.data.DatabaseManager;
import com.travelmap.data.PhotoDetails;
import com.travelmap.screen.DBQueryLoader;

import java.util.Formatter;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.google.maps.android.SphericalUtil.computeDistanceBetween;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TotalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TotalFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<PhotoDetails>>{

    @BindView(R.id.photos_taked_textView)
    TextView mPhotoCountTextView;

    @BindView(R.id.covered_distance_textView)
    TextView mCoveredDistanceTextView;

    private int LOADER_ID=12;

    public TotalFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment
     *
     * @return A new instance of fragment TotalFragment.
     */
    public static TotalFragment newInstance() {
        return new TotalFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_total, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle(getString(R.string.statistics_title));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID, savedInstanceState, this);
    }

    @Override
    public Loader<List<PhotoDetails>> onCreateLoader(int id, Bundle args) {
        return new DBQueryLoader(getContext()) {
            @Override
            protected List<PhotoDetails> runQuery(DatabaseManager databaseManager) {
                return databaseManager.getPhotosPerWeek();
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<List<PhotoDetails>> loader, List<PhotoDetails> data) {
        mPhotoCountTextView.setText(String.valueOf(data.size()));

        double distance=0;
        for (int i=0; i<data.size()-2; i++){
            LatLng p1 = new LatLng(data.get(i).latitude, data.get(i).longitude);
            LatLng p2 = new LatLng(data.get(i+1).latitude, data.get(i+1).longitude);
            distance+=computeDistanceBetween (p1, p2);
        }
        Formatter formatter = new Formatter();
        if(distance<1000000)
            formatter.format("%1$.2f", distance/1000);
        else formatter.format("%1$.0f", distance/1000);
        mCoveredDistanceTextView.setText(formatter.toString());
    }

    @Override
    public void onLoaderReset(Loader<List<PhotoDetails>> loader) {

    }
}

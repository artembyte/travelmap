package com.travelmap.screen.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Artem on 12/27/16.
 */

public class PhotoTaker {

    private String mPhotoFileName;
    public static int REQUEST_IMAGE_CAPTURE = 11;
    public static String TMP_FILE_EXTENSION = ".tmp";
    public static String FULL_FORMAT_PHOTO_FILE_PREFIX = "_";

    public String getPhotoFileName() {
        return mPhotoFileName;
    }

    private File createImageFile(Context context) throws IOException {
        // Create an image file mFileName
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        mPhotoFileName = timeStamp + TMP_FILE_EXTENSION;
        File mStorageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = new File(mStorageDir, mPhotoFileName);
        return image;
    }

    public Intent getTakePictureIntent(Context context) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            // Create the File where the photo should go
            File imFile = null;
            try {
                imFile = createImageFile(context);
            } catch (IOException ex) {
            }
            // Continue only if the File was successfully created
            if (imFile != null) {
                Uri uri = Uri.fromFile(imFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                //Log.d("my", "result");
            }
        }
        return takePictureIntent;
    }
}

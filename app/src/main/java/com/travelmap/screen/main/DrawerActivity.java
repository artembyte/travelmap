package com.travelmap.screen.main;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.travelmap.R;
import com.travelmap.TravelApplication;
import com.travelmap.screen.BaseLocationActivity;
import com.travelmap.screen.camera.CameraFragment;
import com.travelmap.screen.statistics.TotalFragment;

import java.io.File;
import java.io.FilenameFilter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;

public class DrawerActivity extends BaseLocationActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        MapFragment.OnMapFragmentVisibilityChangeListener{

    private static final String FAB_VISIBLE_KEY = "fabKey";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @BindView(R.id.fab)
    FloatingActionButton fab;

    public static final String TOTAL_SCREEN = "total screen";
    public static final String MAP_SCREEN = "map screen";
    public static final String ADD_PHOTO_SCREEN = "add photo screen";
    private PhotoTaker mPhotoTaker;
    private int mFabVisible;
    private FragmentManager mFragmentManager=getSupportFragmentManager();

    private Navigator navigator = new SupportFragmentNavigator(getSupportFragmentManager(),
            R.id.acivity_container){

        @Override
        protected Fragment createFragment(String screenKey, Object data) {

            switch(screenKey) {
                case TOTAL_SCREEN:
                    return TotalFragment.newInstance();
                case MAP_SCREEN:
                    return MapFragment.newInstance();
                case ADD_PHOTO_SCREEN:
                    return CameraFragment.newInstance((String) data);
                default:
                    throw new RuntimeException("Unknown screen key!");
            }
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(DrawerActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        ButterKnife.bind(this);

        if(savedInstanceState==null)
            TravelApplication.INSTANCE.getRouter().newRootScreen(MAP_SCREEN);
        else {
            mFabVisible = savedInstanceState.getInt(FAB_VISIBLE_KEY);
            if(mFabVisible ==View.VISIBLE)
                fab.setVisibility(View.VISIBLE);
            else fab.setVisibility(View.INVISIBLE);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @OnClick(R.id.fab)
    public void startCamApp(View view) {
        Intent photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (photoIntent.resolveActivity(getPackageManager()) != null) {
            mPhotoTaker = new PhotoTaker();
            startActivityForResult(mPhotoTaker.getTakePictureIntent(DrawerActivity.this),
                    PhotoTaker.REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            TravelApplication.INSTANCE.getRouter().exit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * This method compare visible component (activity, fragment) name with
     * new component name that user want to start
     */
    private boolean isEqualsWithVisibleComponentName(String screenKey) {
        int lastAddedToBackStackComponentPos = mFragmentManager.getBackStackEntryCount() - 1;

        return lastAddedToBackStackComponentPos > -1 &&
                mFragmentManager.getBackStackEntryAt(lastAddedToBackStackComponentPos)
                        .getName().equals(screenKey);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_map) {
            TravelApplication.INSTANCE.getRouter().backTo(MAP_SCREEN);
        } else if (id == R.id.nav_statistics) {

            // if this fragment visible do nothing
            if(!isEqualsWithVisibleComponentName(TOTAL_SCREEN))
                TravelApplication.INSTANCE.getRouter().newScreenChain(TOTAL_SCREEN, null);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PhotoTaker.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK
                && mPhotoTaker !=null) {
            TravelApplication.INSTANCE.getRouter()
                    .newScreenChain(ADD_PHOTO_SCREEN, mPhotoTaker.getPhotoFileName());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(FAB_VISIBLE_KEY, mFabVisible);
    }

    @Override
    protected void onResume() {
        super.onResume();
        TravelApplication.INSTANCE.getNavigatorHolder().setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        TravelApplication.INSTANCE.getNavigatorHolder().removeNavigator();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // delete .tmp photo files
        if(isFinishing()) {
            new DeleteTmpPhotoFiles().execute();
        }
    }

    @Override
    public void onFragmentVisibleChanged(int visible) {
        fab.setVisibility(visible);
        mFabVisible = visible;
    }

    private class DeleteTmpPhotoFiles extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            File folder = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File[] files = folder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept( final File dir, final String name ) {
                    return name.endsWith( PhotoTaker.TMP_FILE_EXTENSION );
                }
            } );
            if(files!=null)
                for ( final File file : files )
                    file.delete();
            return null;
        }
    }
}

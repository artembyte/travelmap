package com.travelmap.screen.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.travelmap.R;
import com.travelmap.data.DatabaseManager;
import com.travelmap.data.PhotoDetails;
import com.travelmap.screen.DBQueryLoader;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnMapFragmentVisibilityChangeListener} interface
 * to handle interaction events.
 * Use the {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback , LoaderManager.LoaderCallbacks<List<PhotoDetails>> {

    private GoogleMap mMap;
    private boolean mapReady;
    private static View rootView;

    private OnMapFragmentVisibilityChangeListener mListener;
    private PhotoCluster mCluster;

    private String TILT_KEY="tilt";
    private String LATITUDE_KEY="latit";
    private String LONGITUDE_KEY="longit";
    private String ZOOM_KEY="zoom";
    private String DESCR_LAYOUT_VISIBLE_KEY="visile";
    private String COMMENT_KEY="comm";
    private String DATE_KEY="datek";
    private String PHOTO_PATH_KEY="photok";

    private LatLng mLocation;
    private float mZoom;
    private float mTilt;
    private int LOADER_ID=11;
    private List<PhotoDetails> photoDetailsList=null;
    private String mStorageDir;
    private PhotoDetails mSavedPhotoDetails;

    @BindView(R.id.description_layout)
    LinearLayout mDescrLayout;

    @BindView(R.id.description_photo_ImageView)
    ImageView mDescrPhotoImageView;

    @BindView(R.id.description_comment_TextView)
    TextView mDescrCommentTextView;

    @BindView(R.id.description_date_TextView)
    TextView mDescrDateTextView;
    private AsyncTask<String, Void, Bitmap> mPhotoLoader;

    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapFragment.
     */
    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null){
            // restore map settings
            double latit=savedInstanceState.getDouble(LATITUDE_KEY, 51.3);
            double longit=savedInstanceState.getDouble(LONGITUDE_KEY, 31);
            mLocation = new LatLng(latit, longit);
            mZoom=savedInstanceState.getFloat(ZOOM_KEY, 8);
            mTilt=savedInstanceState.getFloat(TILT_KEY, 30);

            //restore photo description layout settings
            if(savedInstanceState.containsKey(DESCR_LAYOUT_VISIBLE_KEY) ){
                mSavedPhotoDetails = new PhotoDetails();
                mSavedPhotoDetails.comment = savedInstanceState.getString(COMMENT_KEY, "");
                mSavedPhotoDetails.addedDate = new Date(savedInstanceState.getLong(DATE_KEY,
                        System.currentTimeMillis()));
                mSavedPhotoDetails.photoName = savedInstanceState.getString(PHOTO_PATH_KEY, "");
            }
        }
        else {
            mLocation = new LatLng(51.3, 31);
            mZoom = 8;
            mTilt = 30;
        }
        mStorageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = inflater.inflate(R.layout.fragment_map, container, false);
        } catch (InflateException e) {
        /* map is already there, just return rootView as it is */
        }
        getActivity().setTitle(getString(R.string.app_name));
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMapFragmentVisibilityChangeListener) {
            mListener = (OnMapFragmentVisibilityChangeListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMapFragmentVisibilityChangeListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map))
                .getMapAsync(this);
        getLoaderManager().initLoader(LOADER_ID, savedInstanceState, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener != null) {
            mListener.onFragmentVisibleChanged(View.VISIBLE);
        }
    }

    @Override
    public void onPause() {
        if(mMap!=null){
            mLocation = new LatLng(mMap.getCameraPosition().target.latitude,
                    mMap.getCameraPosition().target.longitude);
            mZoom = mMap.getCameraPosition().zoom;
            mTilt = mMap.getCameraPosition().tilt;
        }
        super.onPause();
        if (mListener != null) {
            mListener.onFragmentVisibleChanged(View.INVISIBLE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if(mPhotoLoader!=null && !mPhotoLoader.isCancelled()){
            mPhotoLoader.cancel(true);
        }
    }

    // show description layout
    private PhotoCluster.OnClickListener onItemClickListener = new PhotoCluster.OnClickListener() {
        @Override
        public void onClick(PhotoDetails item) {
            mSavedPhotoDetails=item;

            mDescrLayout.setVisibility(View.VISIBLE);

            // print photo comment
            mDescrCommentTextView.setText(item.comment);

            // print modify/create date
            String date = DateFormat.getDateTimeInstance(
                    DateFormat.MEDIUM, DateFormat.SHORT).format(item.addedDate);
            mDescrDateTextView.setText(date);

            // draw full-format photo
            File storedPhoto = new File(mStorageDir, PhotoTaker
                    .FULL_FORMAT_PHOTO_FILE_PREFIX+item.photoName);

            if(mPhotoLoader!=null && !mPhotoLoader.isCancelled())
                mPhotoLoader.cancel(true);
            mPhotoLoader = new MapFragment.LoadPhoto().execute(storedPhoto.getPath());
        }
    };

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        mapReady=true;

        CameraPosition position = CameraPosition.builder().target(mLocation).zoom(mZoom).tilt(mTilt).build();

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(position));

        // hide description layout if user clicked on map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mDescrLayout.setVisibility(View.GONE);
                mSavedPhotoDetails=null;
            }
        });

        // if description layout settings restored after changing configuration apply it
        if(mSavedPhotoDetails!=null)
            onItemClickListener.onClick(mSavedPhotoDetails);

        if(mCluster!=null)
            mCluster.resetCluster(mMap, getActivity(), photoDetailsList);
        else {
            mCluster = new PhotoCluster(mMap, getActivity(), onItemClickListener);
            mCluster.startDrawMarkers(photoDetailsList);
        }
    }

    // save camera position
    public void onSaveInstanceState(Bundle outState) {
        if(mapReady && mMap!=null) {
            // save map settings
            outState.putDouble(LATITUDE_KEY, mMap.getCameraPosition().target.latitude);
            outState.putDouble(LONGITUDE_KEY, mMap.getCameraPosition().target.longitude);
            outState.putFloat(ZOOM_KEY, mMap.getCameraPosition().zoom);
            outState.putFloat(TILT_KEY, mMap.getCameraPosition().tilt);

            //save photo description settings
            if(mDescrLayout.getVisibility()==View.VISIBLE){
                outState.putBoolean(DESCR_LAYOUT_VISIBLE_KEY, true);
                outState.putString(COMMENT_KEY, mSavedPhotoDetails.comment);
                outState.putLong(DATE_KEY, mSavedPhotoDetails.addedDate.getTime());
                outState.putString(PHOTO_PATH_KEY, mSavedPhotoDetails.photoName);
            }
        }
    }

    @Override
    public Loader<List<PhotoDetails>> onCreateLoader(int id, Bundle args) {
        return new DBQueryLoader(getContext()) {
            @Override
            protected List<PhotoDetails> runQuery(DatabaseManager databaseManager) {
                return databaseManager.getAll();
            }
        };
    }

    /**
     * if this method called not first time, loader data compared with existing list.
     * RemoveList contains items that must be removed from cluster,
     * addList contains items that must be added to cluster
     */
    @Override
    public void onLoadFinished(Loader<List<PhotoDetails>> loader, List<PhotoDetails> data) {
        if(photoDetailsList!=null ){

            List<PhotoDetails> removeList = new ArrayList<>();
            removeList.addAll(photoDetailsList);
            removeList.removeAll(data);

            List<PhotoDetails> addList = new ArrayList<>();
            addList.addAll(data);
            addList.removeAll(photoDetailsList);

            if (removeList.size()>0 || addList.size()>0) {
                photoDetailsList = data;
                if (mCluster != null)
                    mCluster.updateItems(removeList, addList);
            }
        }
        else {
            if (mCluster != null)
                mCluster.addItems(data);
            photoDetailsList=data;
        }
    }

    @OnClick(R.id.description_photo_ImageView)
    void showPhotoUsingIntent(){
        if(mSavedPhotoDetails!=null) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            File storedPhoto = new File(mStorageDir, PhotoTaker
                    .FULL_FORMAT_PHOTO_FILE_PREFIX + mSavedPhotoDetails.photoName);
            intent.setDataAndType(Uri.fromFile(storedPhoto), "image/*");
            startActivity(intent);
        }
    }

    private class LoadPhoto extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (mDescrPhotoImageView != null) {
                mDescrPhotoImageView.setImageBitmap(bitmap);
            }
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return BitmapFactory.decodeFile(params[0]);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<PhotoDetails>> loader) {
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow change visibility of FloatingActionButton that starts
     * this fragment
     */
    public interface OnMapFragmentVisibilityChangeListener {
        // TODO: Update argument type and mFileName
        void onFragmentVisibleChanged(int visible);
    }

}

package com.travelmap.screen.main;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.fitness.ConfigApi;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.algo.GridBasedAlgorithm;
import com.google.maps.android.clustering.algo.NonHierarchicalDistanceBasedAlgorithm;
import com.google.maps.android.clustering.algo.PreCachingAlgorithmDecorator;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.travelmap.R;
import com.travelmap.data.DatabaseHelper;
import com.travelmap.data.PhotoDetails;
import com.travelmap.screen.camera.CameraFragment;

import java.sql.SQLException;
import java.util.List;

import butterknife.OnClick;

/**
 * Created by Artem on 1/10/17.
 */

public class PhotoCluster implements
        ClusterManager.OnClusterClickListener<PhotoDetails>,
        ClusterManager.OnClusterInfoWindowClickListener<PhotoDetails>,
        ClusterManager.OnClusterItemClickListener<PhotoDetails>,
        ClusterManager.OnClusterItemInfoWindowClickListener<PhotoDetails>{
    
    private GoogleMap mMap;
    private ClusterManager<PhotoDetails> mClusterManager;
    private Context mContext;

    private OnClickListener onClickListener;
    interface OnClickListener{
        public void onClick(PhotoDetails item);
    }

    // Context must be Activity
    public PhotoCluster(GoogleMap map, Context context, OnClickListener listener) {
        mMap = map;
        mContext = context;
        onClickListener = listener;
    }

    private class PhotoDetailsRenderer extends DefaultClusterRenderer<PhotoDetails> {
        private final IconGenerator mIconGenerator = new IconGenerator(mContext);
        private final IconGenerator mClusterIconGenerator = new IconGenerator(mContext);
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;
        String mStorageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                .getPath()+"/";

        public PhotoDetailsRenderer() {
            super(mContext, mMap, mClusterManager);

            View multiProfile = ((Activity)mContext).getLayoutInflater().inflate(R.layout.multi_photo, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(mContext);
            mDimension = (int) mContext.getResources().getDimension(R.dimen.custom_photo_thumb);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) mContext.getResources().getDimension(R.dimen.custom_thumb_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(PhotoDetails photoDetails, MarkerOptions markerOptions) {
            // Draw a single photo.
            mImageView.setImageBitmap(BitmapFactory.decodeFile(mStorageDir+photoDetails.photoName));
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<PhotoDetails> cluster, MarkerOptions markerOptions) {

            // draw first photo from cluster
            mClusterImageView.setImageBitmap(BitmapFactory
                    .decodeFile(mStorageDir + cluster.getItems().iterator().next().photoName));

            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // always render clusters.
            return cluster.getSize() > 1;
        }
    }

    @Override
    public boolean onClusterClick(Cluster<PhotoDetails> cluster) {
        
        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items
        // inside of bounds, then animate to center of the bounds.

        // Create the builder to collect all essential cluster items for the bounds.
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            builder.include(item.getPosition());
        }
        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<PhotoDetails> cluster) {
    }

    @Override
    public boolean onClusterItemClick(PhotoDetails item) {
        if(onClickListener!=null)
            onClickListener.onClick(item);
        return true;
    }

    @Override
    public void onClusterItemInfoWindowClick(PhotoDetails item) {
    }

    public void startDrawMarkers(List<PhotoDetails> photoDetailsList) {

        mClusterManager = new ClusterManager<>(mContext, mMap);
        mClusterManager.setRenderer(new PhotoDetailsRenderer());
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItems(photoDetailsList);
    }

    public void resetCluster(GoogleMap map, Context context, List<PhotoDetails> photoDetailsList){
        mClusterManager.clearItems();
        mMap = map;
        mContext = context;
        startDrawMarkers(photoDetailsList);
    }

    public void addItems(List<PhotoDetails> photoDetailsList) {

        if(photoDetailsList!=null) {
            mClusterManager.addItems(photoDetailsList);
            mClusterManager.cluster();
        }
    }

    public void updateItems(List<PhotoDetails> removeList, List<PhotoDetails> addList){
        if(removeList.size()>0) {
            for (PhotoDetails photo : removeList) {
                mClusterManager.removeItem(photo);
            }
            mClusterManager.cluster();
        }

        if(addList.size()>0) {
            if (addList.size() > 0)
                mClusterManager.addItems(addList);
            mClusterManager.cluster();
        }
    }
}

package com.travelmap.screen.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.text.Editable;

import com.travelmap.data.DatabaseManager;
import com.travelmap.screen.main.PhotoTaker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by Artem on 1/20/17.
 */
class PhotoSaver extends AsyncTask<Object, Void, Void> {

    @Override
    protected Void doInBackground(Object... params) {

        Location location = (Location) params[0];
        String tempPhotoName = (String) params[2];
        Bitmap photoBitmap = (Bitmap) params[3];
        File mStorageDir = (File) params[4];

        // rename photo file from .tmp to .jpg
        String photoName = tempPhotoName.substring(0, 15) + ".jpg";

        File file = new File(mStorageDir, tempPhotoName);
        File file2 = new File(mStorageDir, PhotoTaker.FULL_FORMAT_PHOTO_FILE_PREFIX + photoName);
        file.renameTo(file2);

        if (photoBitmap == null) {
            File storedPhoto = new File(mStorageDir, PhotoTaker.FULL_FORMAT_PHOTO_FILE_PREFIX + photoName);
            photoBitmap = BitmapFactory.decodeFile(storedPhoto.getPath());
        }

        // delete enter symbol in the begin of sequence
        Editable comment = (Editable) params[1];
        if (comment.length() > 0)
            while (comment.charAt(0) == '\n')
                comment.replace(0, 1, "");

        // create thumbnail
        int dp = 60; //dimen.custom_photo_thumb
        Bitmap thumbnail = ThumbnailUtils.extractThumbnail(photoBitmap, dp, dp);

        // save thumbnail
        File thumbFile = new File(mStorageDir, photoName);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(thumbFile.getPath());
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        DatabaseManager.getInstance().createPhoto(photoName, location.getLatitude(),
                location.getLongitude(), new Date(), comment.toString());

        return null;
    }
}

package com.travelmap.screen.camera;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.travelmap.LocationService;
import com.travelmap.R;
import com.travelmap.TravelApplication;
import com.travelmap.screen.BaseLocationActivity;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CameraFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CameraFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PHOTO_FILE_NAME = "param1";
    private String IS_PROGRESS_DIALOG_SHOWING_KEY ="showKey";

    @BindView(R.id.add_new_image_imageView)
    ImageView newPhotoImageView;

    @BindView(R.id.new_photo_descr_editText)
    EditText commentEditText;

    private String mTmpPhotoName;
    private File mStorageDir;
    private AsyncTask<String, Void, Bitmap> mPhotoLoader;
    private Bitmap photoBitmap;
    private PropertyChangeListener mLocationUpdateListener;
    public ProgressDialog mProgressDialog;

    public CameraFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment CameraFragment.
     */
    public static CameraFragment newInstance(String param1) {
        CameraFragment fragment = new CameraFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PHOTO_FILE_NAME, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTmpPhotoName = getArguments().getString(ARG_PHOTO_FILE_NAME);
        }

        mStorageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        // show loading progress dialog if it showed before changing orientation
        if (savedInstanceState != null &&
                savedInstanceState.containsKey(IS_PROGRESS_DIALOG_SHOWING_KEY) &&
                savedInstanceState.getBoolean(IS_PROGRESS_DIALOG_SHOWING_KEY))
                showProgressDialog();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (mTmpPhotoName != null) {
            File storedPhoto = new File(mStorageDir, mTmpPhotoName);
            mPhotoLoader = new LoadPhoto().execute(storedPhoto.getPath());
        }

        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle(getString(R.string.new_photo_title));

        return view;
    }

    @OnClick(R.id.add_new_photo_button)
    void saveNewPhoto() {

        if (((BaseLocationActivity)getActivity()).isLocationActual()) {
            Location location = ((BaseLocationActivity) getActivity()).getLastKnownLocation();
            new PhotoSaver().execute(location, commentEditText.getText(), mTmpPhotoName,
                    photoBitmap, mStorageDir);
            onAddButtonPressed();
        }
        else {
            if(mLocationUpdateListener==null) {
                mLocationUpdateListener = new PropertyChangeListener() {

                    @Override
                    public void propertyChange(PropertyChangeEvent event) {

                        if (event.getPropertyName().equals(LocationService.LOCATION_CHANGED_EVENT)) {

                            if(event.getNewValue() instanceof Location)
                                new PhotoSaver().execute(event.getNewValue(), commentEditText.getText(), mTmpPhotoName,
                                        photoBitmap, mStorageDir);

                            ((BaseLocationActivity) getActivity()).unregisterLocationChangeListener(this);
                            mLocationUpdateListener = null;
                            hideProgressDialog();
                            onAddButtonPressed();
                        } else if (event.getPropertyName().equals(LocationService.LOCATION_TRACKING_STARTED_EVENT)) {
                            showProgressDialog();
                        }
                    }
                };
                ((BaseLocationActivity) getActivity()).registerLocationChangeListener(mLocationUpdateListener);
            }
        }
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.wait_for_location));
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    // if add button pressed - close fragment
    private void onAddButtonPressed() {
        TravelApplication.INSTANCE.getRouter().exit();
    }

    // class to load photo async.
    private class LoadPhoto extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (newPhotoImageView != null) {
                CameraFragment.this.photoBitmap=bitmap;
                newPhotoImageView.setImageBitmap(bitmap);
            }
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return BitmapFactory.decodeFile(params[0]);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mProgressDialog!=null)
            outState.putBoolean(IS_PROGRESS_DIALOG_SHOWING_KEY, mProgressDialog.isShowing());
    }

    public void onDestroy() {
        super.onDestroy();
        if(mPhotoLoader!=null && mPhotoLoader.getStatus().equals(AsyncTask.Status.RUNNING)){
            mPhotoLoader.cancel(true);
        }
        // unregister from location updates
        if(mLocationUpdateListener!=null)
            ((BaseLocationActivity) getActivity())
                    .unregisterLocationChangeListener(mLocationUpdateListener);
    }
}

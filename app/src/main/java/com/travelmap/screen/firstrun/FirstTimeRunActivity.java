package com.travelmap.screen.firstrun;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.travelmap.screen.main.DrawerActivity;

/**
 * Created by Artem on 1/31/17.
 */

public class FirstTimeRunActivity extends Activity {
    public static String FIRST_TIME_PREF_NAME = "FirstRunSettings";
    public static String IS_FIRST_TIME_PREF_KEY = "FirstTime";
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        prefs = getSharedPreferences(FIRST_TIME_PREF_NAME, MODE_PRIVATE);
        if (prefs.getBoolean(IS_FIRST_TIME_PREF_KEY, true)) {
            Intent i = new Intent(FirstTimeRunActivity.this, SplashActivity.class);
            startActivity(i);
            finish();
        } else {
            //start main Activity
            Intent mainIntent = new Intent(getApplicationContext(), DrawerActivity.class);
            startActivity(mainIntent);
            finish();
        }
    }
}

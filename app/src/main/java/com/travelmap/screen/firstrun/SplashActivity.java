package com.travelmap.screen.firstrun;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.travelmap.R;
import com.travelmap.data.DatabaseManager;
import com.travelmap.screen.main.DrawerActivity;

public class SplashActivity extends AppCompatActivity {

    SharedPreferences prefs;
    private FirstTimeRunTask firstTimeRunTask;

    /** Called when the activity is first created. */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_time_run);

        firstTimeRunTask = new FirstTimeRunTask();
        firstTimeRunTask.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(firstTimeRunTask.getStatus().equals(AsyncTask.Status.FINISHED))
            startMainActivity();
        else firstTimeRunTask.onActivityResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(firstTimeRunTask.getStatus().equals(AsyncTask.Status.RUNNING))
            firstTimeRunTask.onActivityPause();
    }

    private void startMainActivity(){
        // save first time run settings
        prefs = getSharedPreferences(FirstTimeRunActivity.FIRST_TIME_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(FirstTimeRunActivity.IS_FIRST_TIME_PREF_KEY, false);
        editor.apply();

        // start activity
        Intent mainIntent = new Intent(getApplicationContext(), DrawerActivity.class);
        startActivity(mainIntent);
        finish();
    }

    class FirstTimeRunTask extends AsyncTask<Void, Void, Void> {

        private boolean isActivityVisible =true;

        @Override
        protected Void doInBackground(Void... params) {
            DatabaseManager.getInstance().unpackAssets();
            return null;
        }

        void onActivityPause(){
            isActivityVisible =false;
        }

        void onActivityResume(){
            isActivityVisible =true;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(isActivityVisible) {
                startMainActivity();
            }
        }
    }
}

package com.travelmap.screen;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.travelmap.data.DatabaseManager;
import com.travelmap.data.PhotoDetails;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Created by Artem on 1/20/17.
 */
public abstract class DBQueryLoader extends AsyncTaskLoader<List<PhotoDetails>> {

    private final PropertyChangeListener mListener;
    private List<PhotoDetails> photoList=null;
    private DatabaseManager mDatabaseManager;

    protected DBQueryLoader(Context context) {
        super(context);
        mDatabaseManager = DatabaseManager.getInstance();

        mListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent event) {
                if (event.getPropertyName().equals(DatabaseManager.CONTENT_CHANGED_EVENT)) {
                    DBQueryLoader.super.onContentChanged();
                }
            }
        };

        mDatabaseManager.addContentChangedListener(mListener);
    }

    @Override
    protected void onStartLoading() {
        if (photoList != null)
            deliverResult(photoList);
        if (takeContentChanged() || photoList == null)
            forceLoad();
    }

    @Override
    public List<PhotoDetails> loadInBackground() {
        //photoList = mDatabaseManager.getAll();
        photoList = runQuery(mDatabaseManager);
        return photoList;
    }

    @Override
    protected void onReset() {
        super.onReset();
        mDatabaseManager.removeContentChangedListener(mListener);
    }

    protected abstract List<PhotoDetails> runQuery(DatabaseManager databaseManager);
}

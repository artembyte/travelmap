package com.travelmap.screen;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.travelmap.LocationService;

import java.beans.PropertyChangeListener;

/**
 * This activity created for comfortable communication with LocationService
 *  and retrieve current location. From your activities and fragments you can
 *  call isLocationActual() and getLastKnownLocation() methods
 */

public class BaseLocationActivity extends AppCompatActivity {

    private LocationService mService;
    private boolean mBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent service = new Intent(getApplicationContext(), LocationService.class);
        startService(service);
        bindService(service, mConnection, Context.BIND_AUTO_CREATE);
    }

    public Location getLastKnownLocation(){
        if(mService!=null)
            return mService.getLastKnownLocation();
        else return null;
    }

    public boolean isLocationActual() {
        return mService != null && mService.isLocationActual();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mService!=null){
            mService.startTrackingLocation(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mService!=null){
            mService.stopTrackingLocation(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            LocationService.LocationBinder binder = (LocationService.LocationBinder) service;
            mService = binder.getService();
            mService.startTrackingLocation(BaseLocationActivity.this);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            mService=null;
        }
    };

    public void registerLocationChangeListener(PropertyChangeListener listener){
        if(mService!=null)
            mService.addLocationChangeListener(listener);
    }

    public void unregisterLocationChangeListener(PropertyChangeListener listener){
        if(mService!=null)
            mService.removeLocationChangeListener(listener);
    }

    /**
     * Process result of showed dialog where user can enable or not GPS and Location Services
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode)
        {
            case LocationService.REQUEST_CHECK_SETTINGS:
                switch (resultCode)
                {
                    case Activity.RESULT_OK:
                    {
                        // All required changes were successfully made
                        if(mService!=null)
                            mService.onLocationTrackingEnabled();
                        break;
                    }
                    case Activity.RESULT_CANCELED:
                    {
                        // The user was asked to change settings, but chose not to
                        Toast.makeText(getApplicationContext(), "impossible to add photo without location", Toast.LENGTH_LONG).show();
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
        }
    }
}

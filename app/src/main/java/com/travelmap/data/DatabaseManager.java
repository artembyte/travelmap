package com.travelmap.data;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.DatabaseConnection;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by Artem on 1/20/17.
 */

public class DatabaseManager {

    static private DatabaseManager mInstance;
    private DatabaseHelper mHelper;
    private Context mContext;
    private List<PropertyChangeListener> mContentChangedListeners = new ArrayList<>();
    public static String CONTENT_CHANGED_EVENT="content";

    private DatabaseManager(Context context) {
        mHelper = new DatabaseHelper(context);
        mContext = context;
    }

    static public DatabaseManager getInstance() {
        return mInstance;
    }

    static public void init(Context context) {
        if (mInstance ==null) {
            mInstance = new DatabaseManager(context);
        }
    }

    private DatabaseHelper getHelper() {
        return mHelper;
    }

    public void createPhoto(String photoName, double latitude,
                            double longitude, Date date, String comment) {

        PhotoDetails photDetails = new PhotoDetails(photoName, latitude,
                longitude, date, comment);
        try {
            Dao<PhotoDetails, Integer> photDao = getHelper().getPhotoDao();
            photDao.create(photDetails);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        notifyContentChangedListeners();
    }

    private void notifyContentChangedListeners(){
        for (PropertyChangeListener listener : mContentChangedListeners)
            listener.propertyChange(new PropertyChangeEvent(this, CONTENT_CHANGED_EVENT, null, null));
    }

    /**
     * Creates test db for image files that placed in external storage / DIRECTORY_PICTURES
     */
    public void bulkCreate(){
        String[] names = new String[]{"Walter", "Gran", "Ruth",
                "Stefan", "Mechanic", "Yeats","John", "Trevor the Turtle","Teach"};
        int i=0;

        File folder = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File[] files = folder.listFiles();

        Savepoint savepoint = null;
        Dao<PhotoDetails, Integer> photDao = null;
        DatabaseConnection conn = null;

        try {
            photDao = getHelper().getPhotoDao();

            conn = photDao.startThreadConnection();
            savepoint = conn.setSavePoint(null);

            long maxDate = System.currentTimeMillis();
            long minDate = maxDate-1804800000;

            for(File file: files){
                i++;
                int r=i%9;
                photDao.create(new PhotoDetails(file.getName(), randomLatOrLng(51, 51.8),
                        randomLatOrLng(30.6, 31.4), randomDate(minDate, maxDate), names[r]));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.commit(savepoint);
                photDao.endThreadConnection(conn);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // random to create test db
    private Random mRandom = new Random(1984);

    private double randomLatOrLng(double min, double max) {
        return mRandom.nextDouble() * (max - min) + min;
    }

    private Date randomDate(long min, long max) {
        return new Date( mRandom.nextInt((int) ((max - min)/1000)) * 1000 + min);
    }

    public List<PhotoDetails> getAll() {
        try {
            Dao<PhotoDetails, Integer> photDao = getHelper().getPhotoDao();
            return photDao.queryForAll();
        } catch (SQLException e) {
            return null;
        }
    }

    public List<PhotoDetails> getPhotosPerWeek() {
        try {
            // 604800*1000 is count of milliseconds in week
            QueryBuilder<PhotoDetails, Integer> qb = getHelper().getPhotoDao().queryBuilder();
            qb.where().between(PhotoDetails.DATE_FIELD,
                    new Date(System.currentTimeMillis()-604800*1000), new Date());
            qb.orderBy(PhotoDetails.DATE_FIELD, false);
            return qb.query();
        } catch (SQLException e) {
            return null;
        }
    }

    public void addContentChangedListener(PropertyChangeListener newListener) {
        mContentChangedListeners.add(newListener);
    }

    public void removeContentChangedListener(PropertyChangeListener newListener) {
        mContentChangedListeners.remove(newListener);
    }

    /**
     * Unpack test db and photos from zip assets
     */
    public void unpackAssets(){

        // unpack db
        File dbFile = mContext.getDatabasePath(DatabaseHelper.DATABASE_NAME);
        dbFile.getParentFile().mkdirs();
        unzip("db.zip", dbFile.getParent()+"/");

        // unpack photos
        File folder = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if(folder==null) return;
        File[] files = folder.listFiles();
        if(files.length>0) return;
        unzip("photos.zip", folder.getPath()+"/");
    }

    /**
     * Unzip asset
     */
    private void unzip(String zipAssetName, String pathToSave) {
        try  {
            InputStream zipIn = mContext.getAssets().open(zipAssetName);
            ZipInputStream zin = new ZipInputStream(zipIn);
            ZipEntry ze;
            while ((ze = zin.getNextEntry()) != null) {

                int l=0;
                byte[] buff = new byte[1024];

                FileOutputStream fout = new FileOutputStream(pathToSave + ze.getName());
                while((l=zin.read(buff))>0){
                    fout.write(buff,0, l);
                }

                zin.closeEntry();
                fout.close();
            }
            zin.close();
        } catch(Exception e) {
            Log.e("Decompress", "unzip", e);
        }

    }
}

package com.travelmap.data;

import android.os.Build;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.j256.ormlite.field.DatabaseField;

import java.util.Date;
import java.util.Objects;

/**
 * Created by Artem on 12/13/16.
 */

public class PhotoDetails implements ClusterItem {

    /**
     * Model class for student_details database table
     */
    private static final long serialVersionUID = -222864131214757024L;

    public static final String ID_FIELD = "photo_id";
    public static final String LATITUDE_FIELD = "latitude";
    public static final String LONGITUDE_FIELD = "longitude";
    public static final String DATE_FIELD = "added_date";

    // Primary key defined as an auto generated integer
    // If the database table column mFileName differs than the Model class variable mFileName, the way to map to use columnName
    @DatabaseField(generatedId = true, columnName = ID_FIELD)
    public int photoId;

    // Define a String type field to hold student's mFileName
    @DatabaseField(columnName = "photo_name")
    public String photoName;

    // Define an int type fields to hold location
    @DatabaseField(columnName = LATITUDE_FIELD)
    public double latitude;

    @DatabaseField(columnName = LONGITUDE_FIELD)
    public double longitude;

    // field to hold photos's date of insertion
    @DatabaseField(columnName = DATE_FIELD)
    public Date addedDate;

    // field to hold user's mComment
    @DatabaseField(columnName = "mComment")
    public String comment;

    // Default constructor is needed for the SQLite, so make sure you also have it
    public PhotoDetails(){

    }

    @Override
    public boolean equals(Object o) {
        PhotoDetails details = (PhotoDetails)o;
        return (details.photoId==photoId) && (details.photoName.equals(photoName))
                && (details.latitude==latitude) && (details.longitude==longitude)
                && (details.comment.equals(comment));
    }

    //For our own purpose, so it's easier to create a StudentDetails object
    public PhotoDetails(final String name, final double latitude,
                        final double longitude, final Date date, final String comment){
        this.photoName = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.addedDate = date;
        this.comment = comment;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(latitude, longitude);
    }
}

package com.travelmap;

import android.support.multidex.MultiDexApplication;

import com.travelmap.data.DatabaseManager;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

/**
 * Created by john on 12/24/16.
 */

public class TravelApplication extends MultiDexApplication {

    public static TravelApplication INSTANCE;
    private Cicerone<Router> cicerone;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        cicerone = Cicerone.create();
        DatabaseManager.init(getApplicationContext());
    }

    public NavigatorHolder getNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

    public Router getRouter() {
        return cicerone.getRouter();
    }
}

package com.travelmap;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Many activities may connect to this service. To start tracking location
 * updates from {@link LocationListener} activity must call {@link #startTrackingLocation(Activity)}
 * When all connected activities call {@link #stopTrackingLocation(Activity)}
 * the service wait {@link #CANCEL_LOC_RQE_TIME} milliseconds and if in this time
 * no activity call {@link #startTrackingLocation(Activity)}
 * location updates from {@link LocationListener} stops
 *
 * Service stop self when time between call {@link #stopTrackingLocation(Activity)}
 * and last activity unbind >= {@link #CANCEL_LOC_RQE_TIME}
 *
 * So if to service connected an one activity that called {@link #startTrackingLocation(Activity)}
 * and it changes orientation, service can wait while activity restarts and
 * won't recreated/restarted and even doesn't stop tracking location.
 */

public class LocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks, LocationListener {

    private final int CANCEL_LOC_RQE_TIME = 10000;
    private volatile Location mLastLocation;
    private final IBinder mBinder = new LocationBinder();
    private GoogleApiClient mGoogleApiClient;
    private volatile LocationRequest mLocationRequest;
    // collection of activities that connected to this service
    private final ArrayList<Activity> activities = new ArrayList<>();
    private boolean isTracking;
    private TimerTask mCancelLocTrackTimerTask;
    public static final int REQUEST_CHECK_SETTINGS = 999;
    private List<PropertyChangeListener> mLocationChangeListeners = new ArrayList<>();
    public static final String LOCATION_CHANGED_EVENT = "location";
    public static final String LOCATION_TRACKING_STARTED_EVENT = "tracking";
    private boolean isBound;

    public class LocationBinder extends Binder {
        public LocationService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LocationService.this;
        }
    }

    @Override
    public void onCreate() {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(30 * 1000)
                .setFastestInterval(20 * 1000);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // check permissions to get location
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        checkLocationSettings(true);
    }

    @Override
    public void onLocationChanged(Location location) {
        // notify LocationChangeListeners
        for (PropertyChangeListener name : mLocationChangeListeners)
            name.propertyChange(new PropertyChangeEvent(this, LOCATION_CHANGED_EVENT, mLastLocation, location));
        mLastLocation = location;
    }

    @Override
    public void onConnectionSuspended(int i) {}

    /**
     * Return true if location!=null and call checkLocationSettings()
     */
    public boolean isLocationActual() {
        checkLocationSettings(true);
        return mLastLocation != null;
    }

    /**
     * Should be call from onStop() activity's method
     */
    public void stopTrackingLocation(Activity activity) {
        boolean isEmpty;
        synchronized (activities) {
            activities.remove(activity);
            isEmpty = activities.isEmpty();
        }
        if (isEmpty) {
            // Start task for cancel location tracking if in CANCEL_LOC_RQE_TIME millis
            // this service will not be bound again
            mCancelLocTrackTimerTask = new TimerTask() {
                @Override
                public void run() {
                    stopLocationUpdates();
                    // if no activity bound this service it stop self
                    if(!isBound)
                        stopSelf();
                }
            };
            Timer mCancelLocationTrackTimer = new Timer();
            mCancelLocationTrackTimer.schedule(mCancelLocTrackTimerTask, CANCEL_LOC_RQE_TIME);
        }
    }

    /**
     * Should be call from onStart() activity's method or onServiceConnected()
     * if you using ServiceConnection.
     */
    public void startTrackingLocation(Activity activity) {
        synchronized (activities) {
            activities.add(activity);
        }
        if (mCancelLocTrackTimerTask != null)
            mCancelLocTrackTimerTask.cancel();
        if (!isTracking) checkLocationSettings(false);
    }

    private void startLocationUpdates() {
        if (mGoogleApiClient.isConnected() && !isTracking) {
            // check permissions to get location
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            isTracking = true;
        }
    }

    private void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected() && isTracking)
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        isTracking = false;
    }

    /**
     * Check location settings. If disabled show dialog with prompt to enable it
     */
    private void checkLocationSettings(final boolean showDialog) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        notifyLocationTrackingStarted();
                        startLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        stopLocationUpdates();
                        if (showDialog) {
                            try {
                                synchronized (activities) {
                                    if (!activities.isEmpty())
                                        status.startResolutionForResult(
                                                activities.get(activities.size() - 1),
                                                REQUEST_CHECK_SETTINGS);
                                }
                            } catch (IntentSender.SendIntentException e) {
                            }
                        }
                        break;
                    // we may get this result if airplane mode is on
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        if (showDialog) {
                            synchronized (activities) {
                                if (!activities.isEmpty())
                                    Toast.makeText(activities.get(activities.size() - 1),
                                            R.string.airplane_mode_warning_message, Toast.LENGTH_LONG).show();
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    /**
     * This method called from BaseLocationActivity when user allows to turn on location
     */
    public void onLocationTrackingEnabled() {
        notifyLocationTrackingStarted();
        startLocationUpdates();
    }

    /**
     * notify LocationChangeListeners that location tracking started
     */
    private void notifyLocationTrackingStarted(){
        for (PropertyChangeListener name : mLocationChangeListeners)
            name.propertyChange(new PropertyChangeEvent(this, LOCATION_TRACKING_STARTED_EVENT, null, null));
    }

    public LocationService() {    }

    public void addLocationChangeListener(PropertyChangeListener newListener) {
        mLocationChangeListeners.add(newListener);
    }

    public void removeLocationChangeListener(PropertyChangeListener newListener) {
        mLocationChangeListeners.remove(newListener);
    }

    @Override
    public void onDestroy() {
        if (mCancelLocTrackTimerTask != null)
            mCancelLocTrackTimerTask.cancel();
        if(isTracking)
            stopLocationUpdates();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        mLocationChangeListeners.clear();
    }

    @Override
    public IBinder onBind(Intent intent) {
        isBound = true;
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        isBound=true;
        super.onRebind(intent);
    }

    /**
     * Stop self if location not tracked, in other way
     * mCancelLocationTrackTimer will stop it
     * ( see stopTrackingLocation(Activity activity) method in this class)
     */
    @Override
    public boolean onUnbind(Intent intent) {
        if(!isTracking) {
            stopSelf();
        }
        isBound=false;
        return true;
    }

    public Location getLastKnownLocation() {
        return mLastLocation;
    }
}
